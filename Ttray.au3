
;
; AutoIt Version: 3.0
; Language:       English
; Platform:       Windows 10
; Author:        TW3
;
; Script Function:
;   Simple tray control for Mozilla Thunderbird
;

#Region
#AutoIt3Wrapper_Icon=.\mail.ico
#AutoIt3Wrapper_Outfile=.\Ttray.exe
#AutoIt3Wrapper_Res_Comment=Thunderbird Tray
#AutoIt3Wrapper_Res_Description=Minimise Mozilla Thunderbird to the Windows system tray
#AutoIt3Wrapper_Res_Fileversion=1.0.1
#AutoIt3Wrapper_Res_LegalCopyright=TW3
#AutoIt3Wrapper_Run_Tidy=y
#AutoIt3Wrapper_Run_Au3Stripper=y
#EndRegion
#NoTrayIcon

#include <AutoItConstants.au3> ; Required to hide the window if it is minimised or exit if Thunderbird is closed
#include <TrayConstants.au3> ; Required for the $TRAY_ICONSTATE_SHOW constant.
#include <Misc.au3> ; Required for the single instance behaviour

Opt("TrayMenuMode", 3) ; The default tray menu items will not be shown and items are not checked when selected. These are options 1 and 2 for TrayMenuMode.
tTrayInit() ; The main application entry point

Func tTrayInit()

	_Singleton("Ttray", 0) ; Just exit if Ttray is already running
	AutoItWinSetTitle ("Ttray")
    If WinExists("[REGEXPTITLE:(?i)(.*Mozilla Thunderbird.*); CLASS:MozillaWindowClass; INSTANCE:1]", "") Then
		WinClose("[REGEXPTITLE:(?i)(.*Mozilla Thunderbird.*); CLASS:MozillaWindowClass; INSTANCE:1]", "") ; We can't re-use an existing running instance and _have_ to start the process to grab PID
    EndIf

	Local $iPID = ShellExecute("thunderbird.exe", "", "", "", @SW_HIDE) ; run Thunderbird hidden
    Local $idAs = TrayCreateItem("Autostart")
    Local $idDa = TrayCreateItem("Disable Autostart")
    Local $idSd = TrayCreateItem("Quit")
    Local $idRe = TrayCreateItem("Restore")
	Local $idMs = TrayCreateItem("Minimise")
    ; Local $idEt = TrayCreateItem("Exit Ttray")

    Local Const $sFilePath = @AppDataDir & "\Microsoft\Windows\Start Menu\Programs\Startup\Ttray.lnk" ; Create a constant variable in Local scope of the shortcut filepath.

	if  FileExists($sFilePath) then
		TrayItemDelete($idAs)
		Local $pTgt = 2
	Else
		TrayItemDelete($idDa)
		Local $pTgt = 1
	EndIf

	TrayItemDelete($idMs)
	TraySetClick(16) ; Show the tray menu when right click comes up
    TraySetState($TRAY_ICONSTATE_SHOW) ; Show the tray menu
	TraySetIcon("mail.ico") ; Set the tray menu icon
	TraySetToolTip("Ttray") ; Set the tray menu tooltip

	Local $pTge = 1
	Local $hWnd = WinWait("[REGEXPTITLE:(?i)(.*Mozilla Thunderbird.*); CLASS:MozillaWindowClass; INSTANCE:1]", "") ; Wait until we have Thunderbird up

    While 1

		If WinExists($hWnd, "") Then

			; Retrieve the state of the window using the handle returned by WinWait
			Local $iState = WinGetState($hWnd)

			; Check if the window is minimized
			If BitAND($iState, $WIN_STATE_MINIMIZED) Then
				If $pTge == 2 then
					TrayItemGetState($idMs)
					If WinExists($hWnd, "") Then
						WinSetState($hWnd, "", @SW_HIDE)
						$idMs = TrayItemDelete($idMs)
						$idRe = TrayCreateItem("Restore")
						$pTge = 1
					EndIf
				EndIf
			ElseIf BitAND($iState, $WIN_STATE_VISIBLE) Then
				If $pTge == 1 then
					TrayItemGetState($idRe)
					$idMs = TrayCreateItem("Minimise")
					$idRe = TrayItemDelete($idRe)
					$pTge = 2
				EndIf
			EndIf

		Else
			ExitLoop
		EndIf

        Switch TrayGetMsg()

			Case $TRAY_EVENT_PRIMARYDOWN
				If $pTge == 1 then
					TrayItemGetState($idRe)
					If WinActivate($hWnd, "") Then
						WinSetState($hWnd, "", @SW_SHOW)
						$idMs = TrayCreateItem("Minimise")
						$idRe = TrayItemDelete($idRe)
						$pTge = 2
					EndIf
				Else
					TrayItemGetState($idMs)
					If WinExists($hWnd, "") Then
						WinSetState($hWnd, "", @SW_HIDE)
						$idMs = TrayItemDelete($idMs)
						$idRe = TrayCreateItem("Restore")
						$pTge = 1
					EndIf
				EndIf

            Case $idSd ; Shutdown Ttray and Thunderbird
				ProcessClose($iPID) ; End the process
                ExitLoop

            Case $idRe ; Unhide the Thunderbird window
				TrayItemGetState($idRe)
				If $pTge == 1 then
					If WinActivate($hWnd, "") Then
						WinSetState($hWnd, "", @SW_SHOW)
						$idMs = TrayCreateItem("Minimise")
						$idRe = TrayItemDelete($idRe)
						$pTge = 2
					EndIf
				EndIf

            Case $idMs ; Hide the Thunderbird window
				TrayItemGetState($idMs)
				If $pTge == 2 then
					If WinExists($hWnd, "") Then
						WinSetState($hWnd, "", @SW_HIDE)
						$idMs = TrayItemDelete($idMs)
						$idRe = TrayCreateItem("Restore")
						$pTge = 1
					EndIf
				EndIf

            Case $idDa
				TrayItemGetState($idDa)
				If $pTgt == 2 then
					if  FileExists($sFilePath) then
						FileDelete($sFilePath)
					EndIf
					$idDa = TrayItemDelete($idDa)
					$idAs = TrayCreateItem("Autostart")
					$pTgt = 1
				EndIf

            Case $idAs
				TrayItemGetState($idAs)
				If $pTgt == 1 then
					if  FileExists($sFilePath) then
						; Nothing to do
					Else
						FileCreateShortcut(@ScriptDir & "\Ttray.exe", $sFilePath, "", "", "", "", "", "", "")  ; Create a shortcut
					EndIf
					$idAs = TrayItemDelete($idAs)
					$idDa = TrayCreateItem("Disable Autostart")
					$pTgt = 2
				EndIf

            ; Case $idEt ; Exit just Ttray
                ; ExitLoop

        EndSwitch

    WEnd

EndFunc


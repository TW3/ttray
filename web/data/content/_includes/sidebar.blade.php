<aside>
    <h3>Download</h3>
    <span>Grab the latest version here:</span>
    <br /><br />

    <div class="popover popover-bottom">
        <form style="display: inline" action="@url('assets/dl/'){{ $appArchive }}" method="get"><button class="btn btn-success">Download Ttray <i class="icon icon-link"></i></button></form>
        <div class="popover-container">
            <div class="card bg-dark">
                <div class="card-header">
                    <h3 class="text-light">Warning:</h3>
                </div>
                <div class="card-body"><span class="text-warning bg-dark">Ttray runs on 64 bit Microsoft Windows® only.</span><br /><br /><span class="text-gray">Please <a href="https://gitlab.com/TW3/ttray/issues" class="external-link">report</a> any bugs you find. <b>Thank You!</b></span></div>
                <div class="card-footer"><i class="icon icon-download text-gray"></i></div>
            </div>
        </div>
    </div>

    <br /><br />
    <span>Ttray is licensed under the terms of the <a href="https://gitlab.com/TW3/ttray/blob/master/license.md" class="external-link">GNU General Public License v3</a>.</span>
    <br />
    <span>The sha256 checksum for the file {{ $appArchive }} is:</span>
    <br /><br />
    <div class="bg-dark" id="check-sum"><div class="text-bold"><span class="text-gray"><small>{{ $appChecksum }}</small></span></div></div>
    <br /><hr /><br /><br /><br />
</aside>
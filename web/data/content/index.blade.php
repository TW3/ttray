@extends('_includes.base')
@section('body')

    <div class="welcome">
        <div class="wrapper">
            <h1>Minimise Mozilla Thunderbird to the Windows® system tray</h1>
            <header>
                <span>{{ $siteDescription }}</span>
            </header>
        </div>
    </div>
	<br />
    <div class="left-side"><main>
        @markdown
![Ttray Screen Capture](./assets/images/Ttray-screen-720.png)

**Ttray** runs in the system tray and starts Thunderbird in the background. It has no complex configuration parameters and is simple and easy to install and use. (64-bit Thunderbird version 60 or newer supported.)

### Usage:

- Right clicking on the tray icon will present the option to auto start the application when Windows starts
- A left mouse click on the icon hides and restores the Thunderbird main window
- If you minimise Thunderbird, Ttray will minimise the application window to the Ttray system tray icon and remain working silently out of your way in the background

### Features:

* _Puts Mozilla Thunderbird into the system tray_
* _Has the ability to run at startup_
* _Written in AutoIt script_

        @endmarkdown
    </main></div>

@stop
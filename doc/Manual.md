![Ttray Logo](./images/mail2.png)

# Ttray | User Guide

## Quick Start

 ### Topics:

 This document will cover the following :

 * **Overview**
 * **Recommended Specs**
 * **Downloading Ttray**
 * **Installation**
 * **Running Ttray for the first time**
 * **Auto Start**
 * **Usage tips**

## Overview

**Ttray** is a system tray application which allows you to minimise Mozilla Thunderbird to the system tray.

## Recommended Specs

In most cases, a modern desktop or laptop computer which supports Windows 10 (64 bit) can run Ttray.

Ttray depends upon Mozilla Thunderbird being installed on your Windows 10 system.

<br/><br/>

## Downloading Ttray

Ttray is available as a compiled software program including supporting documents (such as this guide) from the [website][w7v4uhj6] and also as a source code archive. The source code is available to browse on [Gitlab][d4g7tjk3].

Please feel free to modify, compile and redistribute the source code and resulting binaries yourself within the terms of the license. The
[AutoIt][e7d9fae1] docs are a good place to start if you are new to AutoIt.

[w7v4uhj6]: https://tw3.gitlab.io/ttray/ "Ttray website"
[d4g7tjk3]: https://gitlab.com/TW3/ttray "Gitlab Ttray code repository"
[e7d9fae1]: https://www.autoitscript.com/site/autoit/ "AutoIt"

## Installation

Once you have obtained a copy of the software, copy the file **Ttray.exe** into a location somewhere on your computer.
A recommended location to do this is **C:\Programs\Ttray\** but you can save **Ttray.exe** anywhere you like.

If you are running a portable installation of Thunderbird, it is recommended that you copy **Ttray.exe** into the same folder location as thunderbird.exe.

## Running Ttray for the first time

Once **Ttray.exe** has been copied to a folder, navigate into the folder and then double click on the file **Ttray.exe**.

The application will start.

![Windows System Tray](./images/tray.png)

When the application has successfully started, the tray icon will appear in the system tray.

If you cannot see the system tray icon, you will need to disable Windows automatically hiding it. To do so; right click on the task bar:

![Windows System Tray](./images/right-click-option.png)

A menu will appear. Left click on **Taskbar settings**.

![Windows System Tray](./images/notification-area-option.png)

In the settings page which appears, scroll down until the **Select which icons appear on the taskbar** option is visible. Click on **Select which icons appear on the taskbar**.

![Windows System Tray](./images/systray-option.png)

View the list which appears. Find the entry for **Ttray.exe** and click on the option so that it changes to **on**. Close the settings window.

The systray icon should now remain in view whenever the application is running.

## Auto Start

Ttray can start automatically when you start Windows. To enable the auto start option, right click on the Ttray icon and click on **Autostart**.

To disable the autostart option, right click on the Ttray icon and click on **Disable Autostart**.

<br/><br/><br/>

## Usage tips

Right clicking on Ttray will provide the following options:

- Quit
- Minimise/Restore
- Autostart/Disable Autostart

Left clicking on the Ttray icon will toggle hiding or showing the Thunderbird application window.

If you minimise the Thunderbird window using the minimise button in the top right hand corner of the Window, Thunderbird will minimise to tray and not to the taskbar whilst Ttray is running.

If you close/exit Thunderbird, Ttray will also exit.

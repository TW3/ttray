# Ttray

![Ttray Logo](./doc/images/mail2-128x128.png)

## Archived!

### This repo is now archived for educational and historical purposes.

**Ttray is no longer necessary for Thunderbird 78.5 or newer.**

Instead of using Ttray you can now do the following:

- Navigate to **Options** -> **General** -> **System Integration**
- Place a check mark next to "**When Thunderbird is minimised, move to the tray**"
- Close the options tab
- Press the Windows key on the keyboard and the letter '**r**' at the same time
- Paste the following into the run command box and press the enter key `explorer %AppData%\Microsoft\Windows\Start Menu\Programs\Startup\`
- In the explorer window which appears, right click anywhere and create a new shortcut to where **Thunderbird.exe** is installed on your hard drive
- Right click on the shortcut you just created and click on '**Properties**'
- Change the '**Run:**' option to '**Minimised**' and click on OK
- Close the Explorer window

Thunderbird will now start in the tray minimised at startup without the need for Ttray.

## Minimise Mozilla Thunderbird to the Windows® system tray

**Ttray** runs in the system tray and starts Thunderbird in the background.
It has no complex configuration parameters and is simple and easy to install and use. (64-bit Thunderbird version 60 or newer supported.)

![Ttray Screen Capture](./web/data/content/assets/images/Ttray-screen-720.png)

### Usage:

- Right clicking on the tray icon will present the option to auto start the application when Windows starts
- A left mouse click on the icon hides and restores the Thunderbird main window
- If you minimise Thunderbird, Ttray will minimise the application window to the Ttray system tray icon and remain working silently out of your way in the background
<br></br>

### Features:

* _Puts Mozilla Thunderbird into the system tray_
* _Has the ability to run at startup_
* _Written in AutoIt script_

### Releases:

Please visit the [Ttray website](https://tw3.gitlab.io/ttray/) to download a compiled copy of the program.
